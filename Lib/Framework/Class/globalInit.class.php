<?php


class globalInit{

    public function baseUrl($uri = null){
        if($uri){
            return $this->config->baseUrl . $uri ;
        }else{
            return $this->config->baseUrl;
        }
    }

    public function staticUrl($uri = null){
        if($uri){
            return $this->config->staticUrl . $uri ;
        }else{
            return $this->config->staticUrl;
        }
    }

    public function generateCode($length = null, $type = 'auto') {
        if ($type == 'auto') {
            $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
        } elseif ($type == 'l') {
            $char = 'abcdefghijklmnopqrstuvwxyz123456789';
        } elseif ($type == 'u') {
            $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
        }
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $pos = rand(0, strlen($char) - 1);
            $string .= $char{$pos};
        }
        return $string;
    }

    public function setSessionApp($keyword = null, $value = null){
        if($keyword){
            $_SESSION[$keyword] = $value;
        }
    }

}