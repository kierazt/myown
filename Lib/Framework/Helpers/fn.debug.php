<?php

if (!function_exists('debug')) {
    function debug($text) {
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
  
      $trace = debug_backtrace();
      echo "<pre><strong>file: " . $trace[0]['file'] . ", line: " . $trace[0]['line'] . "</strong><br/><hr/>";
      var_dump($text);
  
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        echo '<br/>Page generated in '.$total_time.' seconds.';
        die;
    }
  }