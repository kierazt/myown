<?php

session_start();


require_once 'Config/Autoloader.conf.php';

$init = new globalInit();
$init->baseUrl = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' .'://'. $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
$init->staticUrl = $init->baseUrl . '/static';
$init->development = $config['development'];
$init->appID = $init->generateCode(16);
$init->langID = $init->generateCode(16);
$init->langName = $config['lang'];

//global session generate
//attach appID to session
$init->setSessionApp('appID', $init->appID);

//set lang to session
$init->setSessionApp('langID', $init->langID);
$init->setSessionApp('langName', $init->langName);
