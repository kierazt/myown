<?php

include_once '../Lib/Framework/Helpers/fn.debug.php';
include_once '../App/Config/config.php';
include_once '../App/Config/routes.php';
include_once '../Lib/Framework/Class/parsingUri.class.php';
include_once '../Lib/Framework/Class/globalInit.class.php';
include_once '../App/Config/database.php';
include_once '../App/GlobalControllers.class.php';
include_once '../App/GlobalModels.class.php';
